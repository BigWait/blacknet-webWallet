import React from 'react';
import { Form, Input, Button } from 'antd';
import { FormattedMessage } from '@/utils/locale';
import { checkAccount, checkMnemonic } from '@/utils/helper';


function verifyMessage(message) {
  if (Object.prototype.toString.call(message) === '[object String]' && message.length > 0) {
    return true;
  }
  return false;
}

function verifySign(sign) {
  if (Object.prototype.toString.call(sign) === '[object String]' && sign.length === 128) {
    return true;
  }
  return false;
}

class SignVerifyMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showSignResult: false,
      showVerifyResult: false,
      signResult: '',
      verifyResult: '',
    };
  }

  checkMessage = (rule, value, callback) => {
    if (!verifyMessage(value)) {
      return callback('Invalid message');
    }
    callback();
  }

  checkSign = (rule, value, callback) => {
    if (!verifySign(value)) {
      return callback('Invalid signature');
    }
    callback();
  }

  onSubmitSign = (e) => {
    e.preventDefault();
    this.props.form.validateFields(['sign_mnemonic', 'sign_message'], (err, values) => {
      if (!err) {
        const { sign_mnemonic: mnemonic, sign_message: message } = values;
        const signedMessage = window.blacknetjs.SignMessage(mnemonic, message);
        this.setState({
          showSignResult: true,
          signResult: signedMessage,
        });
      }
    });
  }

  onSubmitVerify = (e) => {
    e.preventDefault();
    this.props.form.validateFields(['verify_account', 'verify_signature', 'verify_message'], (err, values) => {
      if (!err) {
        const { verify_account: account, verify_signature: signature, verify_message: message } = values;
        const result = window.blacknetjs.VerifyMessage(account, signature, message);
        console.log(result);
        this.setState({
          showVerifyResult: true,
          verifyResult: result,
        });
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      showSignResult, signResult, showVerifyResult, verifyResult,
    } = this.state;
    return (
      <div className="sign">
        <div className="form">
          <legend><FormattedMessage id="sign message" /></legend>
          <Form layout="inline">
            <div className="field-row">
              <Form.Item label={<FormattedMessage id="mnemonic" />}>
                {getFieldDecorator('sign_mnemonic', {
                  rules: [{ validator: checkMnemonic }, { transform: (value) => value ? value.trim() : value }],
                  validateTrigger: ['onSubmit', 'onBlur'],
                })(
                  <Input.Password size="large" style={{ width: '494px' }} autoComplete="off" />,
                )}
              </Form.Item>
            </div>
            <div className="field-row">
              <Form.Item label={<FormattedMessage id="message" />}>
                {getFieldDecorator('sign_message', {
                  rules: [{ validator: this.checkMessage }],
                  validateTrigger: ['onSubmit', 'onBlur'],
                })(
                  <Input size="large" style={{ width: '494px' }} autoComplete="off" />,
                )}
              </Form.Item>
            </div>
            <div className="field-row" style={{ display: 'flex', flexDirection: 'row-reverse' }}>
              <Form.Item>
                <Button type="primary" onClick={this.onSubmitSign} style={{ marginRight: '0' }}>
                  <FormattedMessage id="sign message" />
                </Button>
              </Form.Item>
            </div>
          </Form>
        </div>
        <div className="field-row field-row-outer" style={{ display: showSignResult ? 'block' : 'none' }}>
          <label><FormattedMessage id="result" /></label>
          <p id="sign_result">{signResult}</p>
        </div>
        <div className="form">
          <legend><FormattedMessage id="verify message" /></legend>
          <Form layout="inline">
            <div className="field-row">
              <Form.Item label={<FormattedMessage id="account" />}>
                {getFieldDecorator('verify_account', {
                  rules: [{ validator: checkAccount }],
                  validateTrigger: ['onSubmit', 'onBlur'],
                })(
                  <Input size="large" style={{ width: '494px' }} autoComplete="off" />,
                )}
              </Form.Item>
            </div>
            <div className="field-row">
              <Form.Item label={<FormattedMessage id="signature" />}>
                {getFieldDecorator('verify_signature', {
                  rules: [{ validator: this.checkSign }],
                  validateTrigger: ['onSubmit', 'onBlur'],
                })(
                  <Input size="large" style={{ width: '494px' }} autoComplete="off" />,
                )}
              </Form.Item>
            </div>
            <div className="field-row">
              <Form.Item label={<FormattedMessage id="message" />}>
                {getFieldDecorator('verify_message', {
                  rules: [{ validator: this.checkMessage }],
                  validateTrigger: ['onSubmit', 'onBlur'],
                })(
                  <Input size="large" style={{ width: '494px' }} autoComplete="off" />,
                )}
              </Form.Item>
            </div>
            <div className="field-row" style={{ display: 'flex', flexDirection: 'row-reverse' }}>
              <Form.Item>
                <Button type="primary" onClick={this.onSubmitVerify} style={{ marginRight: '0' }}>
                  <FormattedMessage id="verify message" />
                </Button>
              </Form.Item>
            </div>
          </Form>
        </div>
        <div className="field-row field-row-outer" style={{ display: showVerifyResult ? 'block' : 'none' }}>

          <label><FormattedMessage id="result" /></label>
          <p id="verify_result">{verifyResult.toString()}</p>
        </div>
      </div>
    );
  }
}

export default Form.create()(SignVerifyMessage);
