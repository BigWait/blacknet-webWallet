import React from 'react';
import {
  Form, Input, InputNumber, Checkbox, Button, Modal,
} from 'antd';
import MnemonicModal from '_components/MnemonicModal';
import { FormattedMessage } from '@/utils/locale';
import {
  postPromise, checkAccount, checkAmount,
} from '@/utils/helper';

const { BigNumber } = window;
class Send extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mnemonicModalVisible: false,
      mnemonicModalOption: {},
      result: '',
      showResult: false,
    };
  }

  sendMoney = (mnemonic, amount, to, message, encrypted, onSendMoneyCallBack, onCancel) => {
    const fee = 100000;
    const amountText = new BigNumber(amount).toFixed(8);
    amount = new BigNumber(amount).times(1e8).toNumber();

    Modal.confirm({
      title: 'Are you sure you want to send?',
      content: (
        <div>
          <p>{amountText} BLN to </p>
          <p>{to}</p>
          <p>0.001 BLN added as transaction fee?</p>
        </div>
      ),
      onOk() {
        const postdata = {
          mnemonic,
          amount,
          fee,
          to,
          message,
          encrypted,
        };

        postPromise('/transfer', postdata).then((data) => onSendMoneyCallBack(data));
      },

      onCancel() { typeof onCancel === 'function' && onCancel(); },
      getContainer: document.getElementById('root'),
    });
  };

  onSubmitSend = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const {
          transfer_to: to, transfer_amount: amount, transfer_message: message, transfer_encrypted,
        } = values;
        const encrypted = transfer_encrypted ? '1' : '0';
        this.setState({
          mnemonicModalVisible: true,
          mnemonicModalOption: {
            to, amount, message, encrypted,
          },
        });
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      mnemonicModalOption, mnemonicModalVisible, showResult, result,
    } = this.state;
    return (
      <div className="send">
        <div className="form">
          <legend><FormattedMessage id="send bln" /></legend>
          <Form layout="inline" onSubmit={this.onSubmitSend}>
            <div className="field-row">
              <Form.Item label={<FormattedMessage id="pay to" />}>
                {getFieldDecorator('transfer_to', {
                  rules: [{ validator: checkAccount }, { transform: (value) => value ? value.trim() : value }],
                  validateTrigger: ['onSubmit', 'onBlur'],
                })(
                  <Input size="large" style={{ width: '494px' }} autoComplete="off" placeholder="Blacknet account..." />,
                )}
              </Form.Item>
            </div>
            <div className="field-row">
              <Form.Item label={<><span><FormattedMessage id="amount" /></span><strong>(BLN)</strong></>}>
                {getFieldDecorator('transfer_amount', {
                  rules: [{ validator: checkAmount }],
                  validateTrigger: ['onSubmit', 'onBlur'],
                })(
                  <InputNumber size="middle" style={{ width: '514px' }} min={1} step={0.00000001} autoComplete="off" placeholder="Amount" />,
                )}
              </Form.Item>
            </div>
            <div className="field-row">
              <Form.Item label={<FormattedMessage id="message" />}>
                {getFieldDecorator('transfer_message')(
                  <Input size="large" style={{ width: '494px' }} autoComplete="off" placeholder="Transfer message" />,
                )}
              </Form.Item>
            </div>
            <div className="field-row">
              <Form.Item label={<FormattedMessage id="encrypted" />}>
                {getFieldDecorator('transfer_encrypted', {
                  valuePropName: 'checked',
                })(
                  <Checkbox />,
                )}
              </Form.Item>
            </div>
            <div className="field-row">
              <Form.Item label={<><span><FormattedMessage id="fee" /></span><strong>(BLN)</strong></>}>
                {getFieldDecorator('transfer_fee', {
                  validateTrigger: 'onSubmit',
                  initialValue: '0.001',
                })(
                  <Input size="large" style={{ width: '494px' }} autoComplete="off" placeholder="Transfer message" />,
                )}
              </Form.Item>
            </div>
            <div className="field-row" style={{ display: 'flex', flexDirection: 'row-reverse' }}>
              <Form.Item>
                <Button type="primary" htmlType="submit" style={{ marginRight: '0' }}>
                  <FormattedMessage id="send" />
                </Button>
              </Form.Item>
            </div>
          </Form>
        </div>
        <div className="field-row field-row-outer" style={{ display: showResult ? 'block' : 'none' }}>
          <label><FormattedMessage id="result" /></label>
          <p>{result}</p>
        </div>
        <MnemonicModal
          visible={mnemonicModalVisible}
          option={mnemonicModalOption}
          onCancel={() => this.setState({ mnemonicModalVisible: false })}
          onVerifySuccess={(mnemonic) => this.sendMoney(mnemonic, mnemonicModalOption.amount, mnemonicModalOption.to, mnemonicModalOption.message, mnemonicModalOption.encrypted, (data) => {
            this.setState({ mnemonicModalVisible: false, result: data, showResult: true });
          })}
        />
      </div>
    );
  }
}

export default Form.create()(Send);
