import React from 'react';
import {
  Form, Input, InputNumber, Button, message,
} from 'antd';
import MnemonicModal from '_components/MnemonicModal';
import { FormattedMessage } from '@/utils/locale';
import { checkAccount, checkAmount, releaseConfirm } from '@/utils/helper';


const Mini_Lease = 1000;

class Lease extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mnemonicModalVisible: false,
      mnemonicModalOption: {},
      showResult: false,
      result: '',
    };
  }

  onSubmitLease = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { lease_to: to, lease_amount } = values;
        const amount = Number(lease_amount);


        this.setState({
          mnemonicModalVisible: true,
          mnemonicModalOption: { to, amount },
        });
      }
    });
  }

  checkAmount = (rule, value, callback) => {
    if (checkAmount(rule, value, callback, true)) {
      const amount = Number(value);
      if (amount < Mini_Lease) {
        callback(`Lease amount must > ${Mini_Lease} BLN`);
        return;
      }
      callback();
    }
  }

  render() {
    const {
      mnemonicModalVisible, mnemonicModalOption, showResult, result,
    } = this.state;
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="lease">
        <div className="form">
          <legend><FormattedMessage id="lease bln" /></legend>
          <Form layout="inline" onSubmit={this.onSubmitLease}>
            <div className="field-row">
              <Form.Item label={<FormattedMessage id="lease to" />}>
                {getFieldDecorator('lease_to', {
                  rules: [{ validator: checkAccount }, { transform: (value) => value ? value.trim() : value }],
                  validateTrigger: ['onSubmit', 'onBlur'],
                })(
                  <Input size="large" style={{ width: '494px' }} autoComplete="off" placeholder="Blacknet account..." />,
                )}
              </Form.Item>
            </div>
            <div className="field-row">
              <Form.Item label={<><span><FormattedMessage id="amount" /></span><strong>(BLN)</strong></>}>
                {getFieldDecorator('lease_amount', {
                  rules: [{ validator: this.checkAmount }],
                  validateTrigger: ['onSubmit', 'onBlur'],
                })(
                  <InputNumber size="middle" style={{ width: '514px' }} min={1} step={0.00000001} autoComplete="off" placeholder="Amount" />,
                )}
              </Form.Item>
            </div>
            <div className="field-row">
              <Form.Item label={<><span><FormattedMessage id="fee" /></span><strong>(BLN)</strong></>}>
                {getFieldDecorator('lease_fee', {
                  validateTrigger: 'onSubmit',
                  initialValue: '0.001',
                })(
                  <Input size="large" style={{ width: '494px' }} autoComplete="off" placeholder="Transfer message" />,
                )}
              </Form.Item>
            </div>
            <div className="field-row" style={{ display: 'flex', flexDirection: 'row-reverse' }}>
              <Form.Item>
                <Button type="primary" htmlType="submit" style={{ marginRight: '0' }}>
                  <FormattedMessage id="send" />
                </Button>
              </Form.Item>
            </div>
          </Form>
        </div>
        <div className="field-row field-row-outer" style={{ display: showResult ? 'block' : 'none' }}>
          <label><FormattedMessage id="result" /></label>
          <p>{result}</p>
        </div>
        <MnemonicModal
          visible={mnemonicModalVisible}
          option={mnemonicModalOption}
          onCancel={() => this.setState({ mnemonicModalVisible: false })}
          onVerifySuccess={(mnemonic) => releaseConfirm(mnemonic, 'lease', mnemonicModalOption.amount, mnemonicModalOption.to, 0, (data) => {
            this.setState({
              showResult: Boolean(data),
              result: data,
              mnemonicModalVisible: false,
            });
            data && message.info(data);
          })}
        />
      </div>
    );
  }
}

export default Form.create()(Lease);
