import React from 'react';
import { Modal, Input } from 'antd';
import { FormattedMessage } from '@/utils/locale';
import { verifyMnemonic, message } from '@/utils/helper';

export default class MnemonicModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = { visible: true, mnemonicValue: '' };
  }

  static getDerivedStateFromProps(props) {
    const { visible } = props;
    return { visible };
  }

  onOk = async () => {
    const { onVerifySuccess, onOk } = this.props;
    if (typeof onOk === 'function') {
      onOk();
    }
    const { mnemonicValue } = this.state;

    // 验证助记词
    if (!verifyMnemonic(mnemonicValue.trim())) {
      message.warning('Invalid mnemonic');
      return;
    }
    typeof onVerifySuccess === 'function' && onVerifySuccess(mnemonicValue);

    this.onCancel();
  }

  onCancel = () => {
    const { onCancel } = this.props;
    this.setState({ mnemonicValue: '' });
    onCancel();
  }

  onMnemonicChange = (e) => {
    e.persist();
    this.setState(() => ({
      mnemonicValue: e.target.value,
    }));
  }

  render() {
    const { visible, mnemonicValue } = this.state;
    return (
      <Modal
        title={<FormattedMessage id="this operation needs your mnemonic.">This operation needs your Mnemonic.</FormattedMessage>}
        visible={visible}
        onOk={this.onOk}
        onCancel={this.onCancel}
        getContainer={() => document.getElementById('root')}
        cancelButtonProps={{ style: { display: 'none' } }}
        okButtonProps={{ type: undefined }}
        okText={<FormattedMessage id="confirm">Confirm</FormattedMessage>}
        destroyOnClose
      >
        <p><FormattedMessage id="enter your mnemonic:">Enter Your Mnemonic:</FormattedMessage></p>
        <Input.Password style={{ width: '440px' }} onChange={this.onMnemonicChange} allowClear={false} value={mnemonicValue} />
      </Modal>
    );
  }
}
