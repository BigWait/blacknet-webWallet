const host = window.location.hostname === 'localhost' ? 'localhost:8283' : window.location.host;
const url = `ws://${host}/api/v2/websocket`;

let ws;
const resolverMaps = {};

function initWs() {
  if (localStorage.account && !ws) {
    ws = new WebSocket(url);
    ws.onopen = () => {
      const blocks = {
        command: 'subscribe',
        route: 'block',
      };
      const wallet = {
        command: 'subscribe',
        route: 'wallet',
        address: localStorage.account,
      };
      ws.send(JSON.stringify(blocks));
      ws.send(JSON.stringify(wallet));
    };

    ws.onmessage = (message) => {
      if (message.data) {
        const response = JSON.parse(message.data);
        Object.keys(resolverMaps).forEach((k) => {
          const handler = resolverMaps[k];
          typeof handler === 'function' && handler(response);
        });


        // if (response.route == 'block') {
        //   blockStack.push(response.message);
        // } else if (response.route == 'transaction') {
        //   Blacknet.renderTransaction(response.message, true);
        //   if (tx.time * 1000 > Date.now() - 1000 * 60) {
        //     Blacknet.newTransactionNotify(response.message);
        //   }
        // }
      }
    };
  }
}
initWs();


export default {
  ws,

  pushDataResolver(resolver, name) {
    if (typeof resolver === 'function' && !resolverMaps[name]) {
      resolverMaps[name] = resolver;
    }
  },

  destroyDataResolver(name) {
    resolverMaps[name] = undefined;
  },

};
