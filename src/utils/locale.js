import React from 'react';
import { FormattedMessage as FormattedMessageReactIntl } from 'react-intl';
import zh from '../../i18n/zh.json';
import de from '../../i18n/de.json';
import ja from '../../i18n/ja.json';
import sk from '../../i18n/sk.json';
import en from '../../i18n/en.json';

let enMsg = {};
Object.keys(zh).forEach((k) => { enMsg[k] = k; });
enMsg = Object.assign(enMsg, en);

// 初始化
const localeMessage = {
  zh, de, ja, sk, en: enMsg,
};
const lang = navigator.language || navigator.userLanguage;
let locale$0;
if (lang.indexOf('zh') !== -1) {
  locale$0 = 'zh';
} else if (lang.indexOf('ja') !== -1) {
  locale$0 = 'ja';
} else if (lang.indexOf('sk') !== -1) {
  locale$0 = 'sk';
} else if (lang.indexOf('de') !== -1) {
  locale$0 = 'de';
}

const locale = window.location.hostname === 'localhost' ? 'en' : locale$0;

Object.keys(localeMessage[locale]).forEach((key) => {
  localeMessage[locale][key.toLowerCase()] = localeMessage[locale][key];
});

const FormattedMessage = ({ id, children, isText }) => {
  if (!isText) {
    if (children && children.length > 0 && locale === 'en') {
      return <span>{children}</span>;
    }
    if (!localeMessage[locale][id]) {
      return <span>{children}</span>;
    }
    return <FormattedMessageReactIntl id={id} />;
  }
  return localeMessage[locale][id] || id;
};

export { locale, localeMessage, FormattedMessage };
