import React from 'react';
import { Layout } from 'antd';
import { inject, observer } from 'mobx-react';
import { FormattedMessage } from '@/utils/locale';
import TabsPage from '../containers/TabsPage';

const { Header, Content } = Layout;

const PassedDom = () => (
  <div className="container">
    <div className="progress-stats">
      <div className="progress">
        <div className="progress-bar" role="progressbar" />
      </div>
    </div>
    <div className="progress-stats-text" />
  </div>
);

const { BigNumber } = window;

@inject('blacknetStore')
@observer
class HeaderComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { blacknetStore: { network, connections } } = this.props;
    return (
      <>
        <div className="logo">
          Blacknet
        </div>
        <div className="network">
          <p>
            <span><FormattedMessage id="supply" /></span>
            :
            <span className="strong">
              {new BigNumber(network.supply).dividedBy(1e8).toFixed(0) || ''}
              {' '}
            </span>
            <span><FormattedMessage id="height" /></span>
:
            <span className="strong">
              {network.height || ''}
              {' '}
            </span>
            <span><FormattedMessage id="connections" /></span>
:
            <span className="strong">
              {connections || ''}
              {' '}
            </span>
          </p>
        </div>
      </>
    );
  }
}

export default ({ match = {} }) => (
  <Layout>
    <Header>
      <HeaderComponent />
    </Header>
    <Content>
      <TabsPage match={match} />
      <PassedDom />
    </Content>
  </Layout>
);
