import React from 'react';
import { Switch, Route, Router } from 'react-router-dom';
import history from './utils/history';
import AccountPage from './containers/AccountPage';
import Layout from './Layout';

export default (
  () => (
    <Router history={history}>
      <Switch>
        <Route path="/" component={AccountPage} exact />
        <Route path="/:tabName" component={Layout} />
      </Switch>
    </Router>
  )
);
